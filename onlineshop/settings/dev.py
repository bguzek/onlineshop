from .base import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'onlineshop_dev',
        'USER': 'postgres',
        'PASSWORD': 'haslo1234',
        'HOST': 'localhost',
        'PORT': '',
    }
}